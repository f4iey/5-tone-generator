# 5 Tone Generator

Programme permettant de traduire les digits en un son "5 tons" dans les protocoles suivants:
<br>
• ZVEI
<br>
• CCIR
<br>
• EEA
<br>
• EIA
<br>
• EUROSIGNAL

## Installation

Cloner le dépôt dans un répertoire quelconque avec:
```bash
git clone https://gitlab.com/F4IEY/5-tone-generator.git
```

### Utilisation sous MATLAB

Ouvrir le dépôt cloné via *MATLAB/OCTAVE/SCILAB* et taper:
```matlab
clear all
close all
cd MATLAB/
clibuild
```
Suivre les étapes données dans le terminal en rentrant les digits et le protocole souhaités comme demandé.<br>
Pour le protocole, choisir et ecrire *en majuscule* parmi:
1) Protocoles ZVEI:
   * `ZVEI1`
   * `ZVEI2`
   * `ZVEI3`
2) Variantes:
   * `CCIR`
   * `EEA`
   * `EIA`
   * Eurosignal `EURO`

D'autres variantes du ZVEI (comme le DZVEI ou le PDZVEI) seront implémentées prochainement.
Il est également possible d'utiliser `run_gui` au lieu de `clibuild` pour utiliser l'interface graphique intégrée.

### Utilisation standard

~~A VENIR~~