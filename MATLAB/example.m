%Programme d'exemple pour générer du 5 tons en rentrant des digits ou
%ABCDEF en paramètre (F n'est pas disponible pour certains protocoles
%©2020 Jules (F4IEY)
tone = generate5('21414', 'ZVEI1'); %génère le ton de déclenchement urgence VHF(161.300Mhz)
soundsc(tone, 48000);
%audiowrite('5tone.wav', tone, 48000);