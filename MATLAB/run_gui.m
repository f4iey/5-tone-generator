%INTERFACE GRAPHIQUE 5TONS
%(c)2020 Jules F4IEY
clear all
close all
prompt = {'Entrer les digits:','Entrer le protocole:'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'12345','ZVEI1'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
ans1 = char(answer(1));
ans2 = char(answer(2));
tones = generate5(ans1, ans2);
prev = questdlg('Preview sound?');
disp('Processing...');
switch prev
    case 'Yes'
        soundsc(tones, 48000);
end
disp('Compiling WAV file...');
audiowrite(strcat(ans1, '.wav'), tones, 48000);
msgbox('Opération réussie!');
disp('done.');