clear all
close all
fprintf('Welcome in the 5 tone CLI builder!\n(c)2020 Jules F4IEY\n\nEnter numbers to encode in available protocols:\nZVEI1 ZVEI2 ZVEI3 CCIR EEA EIA EURO\nIt is necessary to use one of them\n');
digits = input('digits:','s');
protocol = input('protocol:\n(upper case):', 's');
%demander une confirmation de preview
req = input('preview sound before building [Y/N]?', 's');
fprintf('analyzing and assembling data...\n');
res = generate5(digits, protocol);
if length(res) == 0
    return
end
if strcmp(req, 'y') || strcmp(req, 'Y')
    soundsc(res, 48000);
end
fprintf('building audio...\n');
audiowrite(strcat(digits, '.wav'), res, 48000)
fprintf('Operation Completed!\n');